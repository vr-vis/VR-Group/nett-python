macro (add_event arg1 event_name dir dir_out)

  exec_program( "protoc" "${PROTOBUF_BIN}" ARGS "${dir}/${event_name}.proto" "--proto_path=${dir}" "--cpp_out=${dir}" OUTPUT_VARIABLE output )
  message ("output of protoc is: ${output}")
  message ("${dir}")
      
  exec_program( "protoc" "${PROTOBUF_BIN}" ARGS "${dir}/${event_name}.proto" "--proto_path=${dir}" "--python_out=${dir_out}" OUTPUT_VARIABLE output )

  list (APPEND ${arg1}_EVENT_SOURCE_FILES "${dir}/${event_name}.pb.h" "${dir}/${event_name}.pb.cc")
  list (APPEND EVENT_NAME_LIST ${event_name})
  
#  message ("output off protoc is: ${output}")
#  message ("${dir}")
#  message ("${dir_out}")
endmacro()


macro ( generate_specializations )
    unset ( _EVENT_NAMES )
    foreach ( entry ${EVENT_NAME_LIST} )
    set (_EVENT_NAMES "${_EVENT_NAMES}\nSPECIALIZE_MESSAGE_TYPE( ${entry} )" )
    endforeach()

    configure_file(
	    src/nett-python/specializations_template.h
      specializations.h
	  @ONLY )

    message ( "successfully generated python_slot specializations" )
endmacro()

  
macro ( generate_includes )
    unset ( _EVENT_NAMES )
    foreach ( entry ${EVENT_NAME_LIST} )
    set (_EVENT_NAMES "${_EVENT_NAMES}\n#include <../schema/${entry}.pb.h>" )
    endforeach()

    configure_file(
	    src/nett-python/includes_template.h
      includes.h
	  @ONLY )
    
    message ( "successfully generated event includes" )
            
endmacro()