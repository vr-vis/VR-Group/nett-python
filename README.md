'nett-python' is the "nimble event transport toolkit" python binding allowing to send messages from python to a nett service

Copyright (c) 2014-2017 RWTH Aachen University, Germany, Virtual Reality & Immersive Visualization Group.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program. If not, see [http://www.gnu.org/licenses/].


Requirements:
 * python 2.7
 * nett

How to build:
 * link against nett
 * link against boost-python
 * install six -> pip install six (version 1.10.x)
 * install google protobuf python (see instructions in google protobuf dir /python/README.MD)
