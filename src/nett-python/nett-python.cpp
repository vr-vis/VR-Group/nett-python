#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/implicit.hpp>

#include <nett/nett.h>

#include <memory>
#undef min //oh dear Microsoft, whoever defined min as a macro should be seriously ...

#include <includes.h> //include auto-generated header for all events

using namespace boost::python;

template <class event_type>
class python_slot_out
{
public:
    python_slot_out( std::string const &slot_tag )
    {
        slot_ = nett::make_slot_out< event_type >( slot_tag );
    }

    void send( std::string const & message )
    {
        slot_->send(convert(message));
    }

private:
    event_type convert( std::string const & serialized )
    {
        event_type message;
        if ( !message.ParseFromString( serialized ) )
            throw std::runtime_error( "protobuf ParseFromString() failed" );

        return message;
    }

    std::shared_ptr < nett::slot_out< event_type > > slot_;
};

template <class event_type>
class python_slot_in
{
public:
    python_slot_in()
    {
        slot_ = nett::make_slot_in< event_type >();
    }

    void connect( std::string const & endpoint, std::string const & slot_tag )
    {
        slot_->connect( endpoint, slot_tag );
    }

    void disconnect( std::string const & endpoint, std::string const & slot_tag )
    {
        slot_->disconnect( nett::slot_address( endpoint, slot_tag ) );
    }

    std::string receive()
    {
		event_type message;
		Py_BEGIN_ALLOW_THREADS
        message = slot_->receive();
		Py_END_ALLOW_THREADS

        std::string binary_message;
        if (! message.SerializeToString(&binary_message) )
        {
            throw std::runtime_error( "protobuf SerializeToString() failed" );
        }
        return binary_message;
    }
private:
    std::shared_ptr < nett::slot_in < event_type > > slot_;
};

#define SPECIALIZE_MESSAGE_TYPE( message_name ) \
      class_< python_slot_out < ##message_name > >( "slot_out_"#message_name"", init<std::string>() ) \
     .def( "send", &python_slot_out< ##message_name >::send ) \
     ; \
\
class_< python_slot_in < ##message_name > >( "slot_in_"#message_name"" ) \
    .def( "connect", &python_slot_in< ##message_name >::connect ) \
    .def( "receive", &python_slot_in< ##message_name >::receive ) \
    ;


BOOST_PYTHON_MODULE(nett_python)
{
    def( "initialize", &nett::initialize );
    //def( "shutdown", &nett::shutdown);

    //include auto-generated definitions
    #include "specializations.h"
}
